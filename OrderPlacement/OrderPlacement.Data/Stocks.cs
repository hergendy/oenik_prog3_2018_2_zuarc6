//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OrderPlacement.Data
{
    using System;
    using System.Collections.Generic;
    
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public partial class Stocks
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public Stocks()
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
        {
            this.Orders = new HashSet<Orders>();
        }
    
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public int ItemID { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public Nullable<int> ExpectedShipTimeInDays { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public string ItemDescription { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public int StockAmount { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public virtual ICollection<Orders> Orders { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}
