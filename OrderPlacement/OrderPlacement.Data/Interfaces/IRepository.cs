﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Repository
{
    using System.Linq;

    /// <summary>
    /// Interface for accessing the repository for data
    /// </summary>
    /// <typeparam name="TEntity">Type of entity accessed from the repository</typeparam>
    public interface IRepository<TEntity>
    {
        /// <summary>
        /// Gets all of the results for TEntity
        /// </summary>
        /// <returns>All the entities</returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Inserts a value into the table
        /// </summary>
        /// <param name="entity">Entity to be added</param>
        void Insert(TEntity entity);

        /// <summary>
        /// Removes a value from the table
        /// </summary>
        /// <param name="entity">Enitity to be deleted</param>
        void Remove(TEntity entity);

        /// <summary>
        /// Changes an entity in the table
        /// </summary>
        /// <param name="entity">Entity to change</param>
        /// <param name="field">Number of field</param>
        /// <param name="newfield">Value of field's new value</param>
        void Change(TEntity entity, string field, string newfield);
    }
}
