﻿// <copyright file="IMembersRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Repository.Interfaces
{
    using OrderPlacement.Data;

    /// <summary>
    /// Interface for accessing Members
    /// </summary>
    public interface IMembersRepository : IRepository<Members>
    {
    }
}
