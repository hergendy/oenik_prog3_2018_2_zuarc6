﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Repository
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// Base class for all the repositories
    /// </summary>
    public class Repository
    {
        private static string connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Hergendy\\source\\repos\\OE_PROG3\\OrderPlacement\\OrderPlacement.Data\\OrderPlacementTables.mdf;Integrated Security=True";

        /// <summary>
        /// Connecting to the database
        /// </summary>
        /// <returns>SQLConnection</returns>
        protected IDbConnection GetConnection()
        {
            return new SqlConnection(connectionString);
        }

        /// <summary>
        /// Connecting to the database
        /// </summary>
        /// <param name="dbConnection">Database connection object</param>
        protected void Connect(IDbConnection dbConnection)
        {
            dbConnection.Open();
        }
    }
}
