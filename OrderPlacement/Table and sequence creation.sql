﻿Drop sequence SEQ_Address
Create sequence SEQ_Address
start with 1
increment by 1;
Drop sequence SEQ_Orders
Create sequence SEQ_Orders
start with 1
increment by 1;
Drop sequence SEQ_Stocks 
Create sequence SEQ_Stocks 
start with 1
increment by 1;
Drop sequence SEQ_Members 
Create sequence SEQ_Members 
start with 1
increment by 1;

If OBJECT_ID('Orders', 'U') IS NOT NULL
Drop table Orders;
If OBJECT_ID('Members', 'U') IS NOT NULL
Drop table Members;
If OBJECT_ID('Stocks', 'U') IS NOT NULL
Drop table Stocks;
If OBJECT_ID('Addresses', 'U') IS NOT NULL
Drop table Addresses;

Create table Addresses (
AddressID int IDENTITY(1,1),
AddressDescription varchar(255),
City varchar(255),
Country varchar(255),
DoorNumber varchar(255),
FloorNumber varchar(255),
NumberFrom varchar(255),
NumberTo varchar(255),
Province varchar(255),
Street varchar(255),
StreetExtension varchar(255),
ZIP varchar(255),
Primary Key(AddressID),
Constraint ZIPCheck Check (ZIP >= 1 AND ZIP <= 9999999)
)

Create table Stocks (
ItemID int IDENTITY(1,1) NOT NULL,
ExpectedShipTimeInDays int,
ItemDescription varchar(2000),
StockAmount int NOT NULL,
Primary Key(ItemID)
)

Create table Members (
MemberID int IDENTITY(1,1) NOT NULL,
AddressID int NOT NULL,
Prefix varchar(255),
FirstName varchar(255),
LastName varchar(255),
Birthdate Date,
Status char,
Primary Key(MemberID),
Foreign Key(AddressID) references Addresses(AddressID)
)

Create table Orders (
OrdersID int IDENTITY(1,1) NOT NULL,
MemberID int NOT NULL,
ItemID int NOT NULL,
OrderState char NOT NULL,
GrossValue float NOT NULL,
VAT float,
NetValue float,
Primary Key(OrdersID),
Foreign Key(ItemID) references Stocks(ItemID),
Foreign Key(MemberID) references Members(MemberID)
)

Insert into Addresses(AddressDescription, City, Country, DoorNumber, FloorNumber, NumberFrom, NumberTo, Province, Street, StreetExtension, ZIP) values ('Aulich','Bratislava','Szlovákia',null,null,'29','23','Pozsonyi','Kozia','utca',811)
Insert into Addresses(AddressDescription, City, Country, DoorNumber, FloorNumber, NumberFrom, NumberTo, Province, Street, StreetExtension, ZIP) values ('Török','Gödöllő','Magyarország',null,null,'2','4','Pest','Röges','utca',2100)
Insert into Addresses(AddressDescription, City, Country, DoorNumber, FloorNumber, NumberFrom, NumberTo, Province, Street, StreetExtension, ZIP) values ('Lahner','Necpál','Szlovákia',null,null,'170',null,'Zsolnai','Necpaly','utca',03812)
Insert into Addresses(AddressDescription, City, Country, DoorNumber, FloorNumber, NumberFrom, NumberTo, Province, Street, StreetExtension, ZIP) values ('Schweidel','Zombor','Szerbia',null,null,'1',null,'Vajdaság','Kosovska','utca',25000)
Insert into Addresses(AddressDescription, City, Country, DoorNumber, FloorNumber, NumberFrom, NumberTo, Province, Street, StreetExtension, ZIP) values ('Pöltenberg','Bécs','Ausztria',null,null,'1',null,'Wien','Löwel','straße',11010)
Insert into Addresses(AddressDescription, City, Country, DoorNumber, FloorNumber, NumberFrom, NumberTo, Province, Street, StreetExtension, ZIP) values ('Nagysándor','Nagyvárad','Románia',null,null,'46','40','Bihar','Spartacus','strada',410469)
Insert into Addresses(AddressDescription, City, Country, DoorNumber, FloorNumber, NumberFrom, NumberTo, Province, Street, StreetExtension, ZIP) values ('Knézics','Nagygordonya','Horvátország',null,null,'4a',null,'Belovár-Bilogora','Petra Preradovića','ulica',43270)
Insert into Addresses(AddressDescription, City, Country, DoorNumber, FloorNumber, NumberFrom, NumberTo, Province, Street, StreetExtension, ZIP) values ('Leiningen','Niddatal','Németország',null,null,'10',null,'Hessen','Bahnhof','straße',61194)
Insert into Addresses(AddressDescription, City, Country, DoorNumber, FloorNumber, NumberFrom, NumberTo, Province, Street, StreetExtension, ZIP) values ('Dessewffy','Ósvacsákány','Szlovákia',null,null,'64',null,'Kassai','Čakanovce','utca',04445)
Insert into Addresses(AddressDescription, City, Country, DoorNumber, FloorNumber, NumberFrom, NumberTo, Province, Street, StreetExtension, ZIP) values ('Damjanich','Sztáza','Horvátország',null,null,'79a',null,'Sziszek-Moslavina','Novoselska','utca',44210)
Insert into Addresses(AddressDescription, City, Country, DoorNumber, FloorNumber, NumberFrom, NumberTo, Province, Street, StreetExtension, ZIP) values ('Lázár','Zrenjanin','Szerbia',null,null,'23',null,'Vajdaság','Nikole Tesle','utca',23000)
Insert into Addresses(AddressDescription, City, Country, DoorNumber, FloorNumber, NumberFrom, NumberTo, Province, Street, StreetExtension, ZIP) values ('Rzeczniów','Rzeczniów','Lengyelország',null,null,'2',null,'Mazóvia','Rzeczniówek','utca',27353)
Insert into Addresses(AddressDescription, City, Country, DoorNumber, FloorNumber, NumberFrom, NumberTo, Province, Street, StreetExtension, ZIP) values ('Kiss','Temesvár','Románia',null,null,'67','79','Temes','Eroilor de la Tisa','Bulevardul',300030)
Insert into Members(AddressID, Prefix, FirstName, LastName, Birthdate, Status) values(1,null,'Aulich','Lajos', CAST('19430825' AS DATETIME), 'D')
Insert into Members(AddressID, Prefix, FirstName, LastName, Birthdate, Status) values(2,null,'Török','Ignác', CAST('19450723' AS DATETIME), 'D')
Insert into Members(AddressID, Prefix, FirstName, LastName, Birthdate, Status) values(3,null,'Lahner','György', CAST('19451006' AS DATETIME), 'D')
Insert into Members(AddressID, Prefix, FirstName, LastName, Birthdate, Status) values(4,null,'Schweidel','József', CAST('19460518' AS DATETIME), 'D')
Insert into Members(AddressID, Prefix, FirstName, LastName, Birthdate, Status) values(5,'Lovag','Poelt von Poeltenberg','Ernő', CAST('19460518' AS DATETIME), 'D')
Insert into Members(AddressID, Prefix, FirstName, LastName, Birthdate, Status) values(6,null,'Nagysándor','József', CAST('19530819' AS DATETIME), 'D')
Insert into Members(AddressID, Prefix, FirstName, LastName, Birthdate, Status) values(7,null,'Knezić','Károly', CAST('19580906' AS DATETIME), 'D')
Insert into Members(AddressID, Prefix, FirstName, LastName, Birthdate, Status) values(8,'Gróf','Leiningen-Westerburg','Károly', CAST('19690411' AS DATETIME), 'D')
Insert into Members(AddressID, Prefix, FirstName, LastName, Birthdate, Status) values(9,null,'Dessewffy','Arisztid', CAST('19520702' AS DATETIME), 'D')
Insert into Members(AddressID, Prefix, FirstName, LastName, Birthdate, Status) values(10,null,'Damjanich','János', CAST('19541208' AS DATETIME), 'D')
Insert into Members(AddressID, Prefix, FirstName, LastName, Birthdate, Status) values(11,null,'Lázár','Vilmos', CAST('19671024' AS DATETIME), 'D')
Insert into Members(AddressID, Prefix, FirstName, LastName, Birthdate, Status) values(12,'Gróf','Vécsey','Károly', CAST('19531124' AS DATETIME), 'D')
Insert into Members(AddressID, Prefix, FirstName, LastName, Birthdate, Status) values(13,null,'Kiss','Ernő', CAST('19490713' AS DATETIME), 'D')
Insert into Stocks(ExpectedShipTimeInDays, ItemDescription, StockAmount) values(3,'Lámpa',10)
Insert into Stocks(ExpectedShipTimeInDays, ItemDescription, StockAmount) values(1,'Asztal',13)
Insert into Stocks(ExpectedShipTimeInDays, ItemDescription, StockAmount) values(2,'Éjjeli szekrény',7)
Insert into Stocks(ExpectedShipTimeInDays, ItemDescription, StockAmount) values(6,'Komód',4)
Insert into Stocks(ExpectedShipTimeInDays, ItemDescription, StockAmount) values(12,'Párna',72)
Insert into Stocks(ExpectedShipTimeInDays, ItemDescription, StockAmount) values(9,'Paplan',6)
Insert into Stocks(ExpectedShipTimeInDays, ItemDescription, StockAmount) values(2,'Ágy',8)
Insert into Stocks(ExpectedShipTimeInDays, ItemDescription, StockAmount) values(7,'Lepedő',42)
Insert into Stocks(ExpectedShipTimeInDays, ItemDescription, StockAmount) values(11,'Matrac',17)
Insert into Stocks(ExpectedShipTimeInDays, ItemDescription, StockAmount) values(7,'Üres doboz',26)
Insert into Stocks(ExpectedShipTimeInDays, ItemDescription, StockAmount) values(4,'Megégett melegszendvics',73)
Insert into Stocks(ExpectedShipTimeInDays, ItemDescription, StockAmount) values(6,'Mérgezett alma',26)
Insert into Stocks(ExpectedShipTimeInDays, ItemDescription, StockAmount) values(5,'Gyufásdoboz',134)
Insert into Orders(MemberID, ItemID, OrderState, GrossValue, VAT, NetValue) values(1,1,'F',2500,2500*0.27,2500*1.27)
Insert into Orders(MemberID, ItemID, OrderState, GrossValue, VAT, NetValue) values(2,2,'F',4000,4000*0.27,4000*1.27)
Insert into Orders(MemberID, ItemID, OrderState, GrossValue, VAT, NetValue) values(3,3,'W',3490,3490*0.27,3490*1.27)
Insert into Orders(MemberID, ItemID, OrderState, GrossValue, VAT, NetValue) values(4,4,'W',1000,1000*0.27,1000*1.27)
Insert into Orders(MemberID, ItemID, OrderState, GrossValue, VAT, NetValue) values(5,5,'F',2500,2500*0.27,2500*1.27)
Insert into Orders(MemberID, ItemID, OrderState, GrossValue, VAT, NetValue) values(6,6,'F',4999,4999*0.27,4999*1.27)
Insert into Orders(MemberID, ItemID, OrderState, GrossValue, VAT, NetValue) values(7,7,'F',6000,6000*0.27,6000*1.27)
Insert into Orders(MemberID, ItemID, OrderState, GrossValue, VAT, NetValue) values(8,8,'P',1200,1200*0.27,1200*1.27)
Insert into Orders(MemberID, ItemID, OrderState, GrossValue, VAT, NetValue) values(9,9,'P',25000,25000*0.27,25000*1.27)
Insert into Orders(MemberID, ItemID, OrderState, GrossValue, VAT, NetValue) values(10,10,'P',5999,5999*0.27,5999*1.27)
Insert into Orders(MemberID, ItemID, OrderState, GrossValue, VAT, NetValue) values(11,11,'W',129999,129999*0.27,129999*1.27)
Insert into Orders(MemberID, ItemID, OrderState, GrossValue, VAT, NetValue) values(12,12,'F',4899,4899*0.27,4899*1.27)
Insert into Orders(MemberID, ItemID, OrderState, GrossValue, VAT, NetValue) values(13,13,'F',41999,41999*0.27,41999*1.27)

select * from orders;