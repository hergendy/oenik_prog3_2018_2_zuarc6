﻿// <copyright file="MembersLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Logic
{
    using System;
    using System.Linq;
    using System.Threading;
    using OrderPlacement.Data;
    using OrderPlacement.Repository;
    using OrderPlacement.Repository.Interfaces;

    /// <summary>
    /// Class handling the logic of members
    /// </summary>
    public class MembersLogic
    {
        private IMembersRepository membersRepository = new MembersRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="MembersLogic"/> class.
        /// </summary>
        public MembersLogic()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MembersLogic"/> class.
        /// </summary>
        /// <param name="membersRepository">Repository to access data</param>
        public MembersLogic(IMembersRepository membersRepository)
        {
            this.membersRepository = membersRepository ?? throw new ArgumentNullException(nameof(membersRepository));
        }

        /// <summary>
        /// Returns the amount of members in the database
        /// </summary>
        /// <returns>Number of members</returns>
        public int Count()
        {
            return this.membersRepository.GetAll().Count();
        }

        /// <summary>
        /// Returns all MemberIDs as an array
        /// </summary>
        /// <returns>All MemberIDs</returns>
        public int[] IDs()
        {
            return this.membersRepository.GetAll().Select(x => x.MemberID).ToArray();
        }

        /// <summary>
        /// Gets a parameter from a member
        /// </summary>
        /// <param name="id">Input ID</param>
        /// <param name="field">Parameter to find</param>
        /// <returns>Found parameter</returns>
        public string GetParameter(int id, string field)
        {
            string rst = string.Empty;
            switch (field)
            {
                case "AddressID":
                    rst = this.membersRepository.GetAll().Where(x => x.MemberID == id).Single().AddressID.ToString();
                    break;
                case "Prefix":
                    rst = this.membersRepository.GetAll().Where(x => x.MemberID == id).Single().Prefix.ToString();
                    break;
                case "FirstName":
                    rst = this.membersRepository.GetAll().Where(x => x.MemberID == id).Single().FirstName.ToString();
                    break;
                case "LastName":
                    rst = this.membersRepository.GetAll().Where(x => x.MemberID == id).Single().LastName.ToString();
                    break;
                case "BirthDate":
                    rst = this.membersRepository.GetAll().Where(x => x.MemberID == id).Single().Birthdate.ToString();
                    break;
                case "Status":
                    rst = this.membersRepository.GetAll().Where(x => x.MemberID == id).Single().Status.ToString();
                    break;
                default:
                    break;
            }

            return rst;
        }

        /// <summary>
        /// Creates a new entity
        /// </summary>
        /// <param name="param">Inserted parameteres</param>
        public void New(string[] param)
        {
            Members members = new Members();
            for (int i = 0; i < 6; i++)
            {
                if (param[i] != null && !param[i].Trim(' ').Equals(string.Empty))
                {
                    switch (i)
                    {
                        case 0:
                            try
                            {
                                members.AddressID = int.Parse(param[i]);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                                members = null;
                                i = 6;
                                break;
                            }

                            continue;
                        case 1:
                            members.Prefix = param[i];
                            continue;
                        case 2:
                            members.FirstName = param[i];
                            continue;
                        case 3:
                            members.LastName = param[i];
                            continue;
                        case 4:
                            try
                            {
                                members.Birthdate = new DateTime(int.Parse(param[i].Substring(0, 4)), int.Parse(param[i].Substring(4, 2)), int.Parse(param[i].Substring(6, 2)));
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                Console.WriteLine("Nem megfelelő a születésnap formátuma!");
                                members = null;
                                i = 6;
                                break;
                            }

                            continue;
                        case 5:
                            members.Status = param[i];
                            continue;
                        default:
                            break;
                    }
                }
            }

            if (members != null)
            {
                this.Add(members);
            }
            else
            {
                Console.WriteLine("Új felhasználó hozzáadása nem sikerült!");
                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                {
                }
            }
        }

        /// <summary>
        /// Lists shit
        /// </summary>
        /// <returns>All addresses</returns>
        public string[] List()
        {
            var q = from w in this.membersRepository.GetAll()
                    select w;
            string[] res = new string[q.Count()];
            int i = 0;
            foreach (Members v in q)
            {
                string str = string.Empty;

                str = $"{v.MemberID} {v.AddressID} ";

                if (v.Prefix != null && !v.Prefix.Equals(string.Empty))
                {
                    str += v.Prefix + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.FirstName != null && !v.FirstName.Equals(string.Empty))
                {
                    str += v.FirstName + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.LastName != null && !v.LastName.Equals(string.Empty))
                {
                    str += v.LastName + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.Birthdate != null && !v.Birthdate.Equals(string.Empty))
                {
                    str += v.Birthdate + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.Status != null && !v.Status.Equals(string.Empty))
                {
                    str += v.Status + " ";
                }
                else
                {
                    str += " null ";
                }

                res[i] = str;
                i++;
            }

            return res;
        }

        /// <summary>
        /// Changes an entity in the table
        /// </summary>
        /// <param name="number">ID of entity</param>
        /// <param name="field">Number of field</param>
        /// <param name="newfield">Value of field's new value</param>
        public void Change(string number, string field, string newfield)
        {
            string fie = string.Empty;
            switch (int.Parse(field))
            {
                case 1:
                    fie = "AddressID";
                    break;
                case 2:
                    fie = "Prefix";
                    break;
                case 3:
                    fie = "FirstName";
                    break;
                case 4:
                    fie = "LastName";
                    break;
                case 5:
                    fie = "BirthDate";
                    break;
                case 6:
                    fie = "Status";
                    break;
                default:
                    break;
            }

            Members members = this.membersRepository.GetAll().Where(w => w.AddressID.ToString().Equals(number)).Single();
            this.membersRepository.Change(members, fie, newfield);
        }

        /// <summary>
        /// Removing an address
        /// </summary>
        /// <param name="id">Id of removed address</param>
        public void Remove(int id)
        {
            try
            {
                Members members = this.membersRepository.GetAll().Where(x => x.MemberID == id).Single();
                this.membersRepository.Remove(members);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (e.InnerException != null)
                {
                    if (e.InnerException.InnerException != null)
                    {
                        Console.WriteLine(e.InnerException.InnerException.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Adds a new address to the table
        /// </summary>
        /// <param name="members">Input address</param>
        private void Add(Members members)
        {
            this.membersRepository.Insert(members);
        }
    }
}
