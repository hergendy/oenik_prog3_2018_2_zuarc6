<%-- 
    Document   : szerkeszto
    Created on : Nov 19, 2018, 4:18:43 PM
    Author     : I330322
--%>
<%@page import="Model.Szemely"%>
<%@ taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "x" uri="http://java.sun.com/jsp/jstl/xml" %>
 
<%@ page contentType = "text/xml" %>
<% Szemely szem = (Szemely) request.getAttribute("kurva"); %>
<root>
   <first>
      <id><%= szem.getId() %></id>
      <desc><%= szem.getDesc() %></desc>
      <val><%= szem.getVal(0) %></val>
   </first>
   <second>
      <id><%= szem.getId() %></id>
      <desc><%= szem.getDesc() %></desc>
      <val><%= szem.getVal(1) %></val>
   </second>
   <third>
      <id><%= szem.getId() %></id>
      <desc><%= szem.getDesc() %></desc>
      <val><%= szem.getVal(2) %></val>
   </third>
   <fourth>
      <id><%= szem.getId() %></id>
      <desc><%= szem.getDesc() %></desc>
      <val><%= szem.getVal(3) %></val>
   </fourth>
   <fifth>
      <id><%= szem.getId() %></id>
      <desc><%= szem.getDesc() %></desc>
      <val><%= szem.getVal(4) %></val>
   </fifth>
</root>
